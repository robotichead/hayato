"""
This script will contact the Melbourne City On Street Parking Sensors. This 
script should be run every 2 minutes after completion. Once the data has been
extracted from the API, it will store the data on the mongodb

Method
~~~~~~
1. Import libraries
2. Get imported arguments (i.e. API KEY)
3. Connect to API
4. Extract data from API
5. Connect to database
6. Store data in database
7. Close database
8. Close program

Any errors will be printed to screen - unless email is provided as an argument.
"""
import os
import json
import urllib.request
import datetime
import sys
from pymongo import MongoClient

#Collect variables
#api_key = sys.argv[1] #First argument given
datetime_stamp = str(datetime.datetime.now().strftime('%Y-%m-%d-%H%M%S'))


base_url = r'https://data.melbourne.vic.gov.au/resource/vh2v-4nfs.%s?$limit=%s' \
    % ('json', #Format
        u'100') #Length


#Get data
try:
    result = json.load(urllib.request.urlopen(base_url))
except:
    #It failed.. exit with error
    print("IT FAILED :'(")
    sys.exit()


#Open Database
try:
    client = MongoClient('mongodb://localhost',27017)
except:
    print("Database issue!")
    sys.exit()

#Connect to a particular database
db = client.melb_city_parking

#Getting the posts ready
posts = db.posts

post_data = {
    'database': 'Melbourne City On Street Parking',
    'datetime': datetime_stamp,
    'data': result,
}
post_result = posts.insert_one(result)
#print('One post {0}'.format(post_result.inserted_id))
